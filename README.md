## Instruciones para seguir el desarrollo del proyecto

**Descripcion:** Dado que cada clase estaremos modificando el proyecto agregando
nuevas funcionalidades y modificando existentes con el objetivo ejemplificar los 
conceptos basicos del desarrollo de aplicaciones en android, cada clase se ira 
guarando dentro del mismo historial de git de tal manera que si desean revisar
una clase en particular solo sera necesario correr los siguientes comandos:

```
git log # para revisar el historial de git
git check <COMMIT HASH> # Para ir a un dia en particular, tomar en cuenta que <COMMIT HASH> es el identificador hexadecimal de cada commit
```
